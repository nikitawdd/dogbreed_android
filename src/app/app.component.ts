import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Client } from "../app/forminterfaces";
import { HomePage } from '../pages/home/home';
import { CategoriesPage } from "../pages/categories/categories";
import { DietPage } from '../pages/diet/diet';
import { trainingPage } from '../pages/training/training';
import { EditprofilePage } from "../pages/editprofile/editprofile";
import { LoginPage } from "../pages/login/login";
import { sessionService } from "../app/sessiondata";
import { DietsugessionPage } from "../pages/dietsugession/dietsugession";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any,url:string}>;
  private ClientObj:Client = {id:null, name:"", email:"", imageUrl:"",login:false};
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public session:sessionService, public events: Events,public menuCtrl: MenuController) {
    this.initializeApp();
    events.subscribe('user:created', (user, time) => {
      this.ClientObj = this.session.get();
    });

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Dog Breeds', "url":"assets/imgs/dogbreed.png",component: CategoriesPage },
      { title: 'Training', "url":"assets/imgs/dogtraining.png",component: trainingPage },
      { title: 'Diet Plan', "url":"assets/imgs/dogfood.png",component: DietPage },
      { title: 'Diet Sugession', "url":"assets/imgs/dietchart.png",component: DietsugessionPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.menuCtrl.close();
    this.nav.setRoot(page.component);
  }
  
  settingPage() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.menuCtrl.close();
    this.nav.setRoot(EditprofilePage);
  }

  logout() {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.menuCtrl.close();
    this.ClientObj = {id:null, name:"", email:"", imageUrl:"",login:false};
    this.session.set(this.ClientObj);
    this.nav.setRoot(LoginPage);
  }

}
