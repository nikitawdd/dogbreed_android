export interface Client {
    "id": String,
    "name": String,
    "email": String,
    "imageUrl": String,
    "login": Boolean
}
export interface playlistObj {
    "title": String,
    "description": String,
    "position": Number,
    "thumbnails": thumbnail,
    "resourceId":resourceId
}
export interface thumbnail {
    "default": image,
    "medium": image,
    "high": image,
    "standard": image,
    "maxres": image,
}
export interface image {
    "url": String,
    "width": Number,
    "height": Number
}
export interface resourceId {
    "videoId": String
}
export interface breeds {
    "id": Number,
    "name":String,
    "discription":String,
    "url":String,
    "lifespan":String,
    "temperment":String,
    "height":String,
    "weight":String,
    "colors":String,
    "origin":String
}
export interface healthyfood {
    "id": Number,
    "discription":String,
    "url":String
}
export interface howmuchfood {
    "id": Number,
    "title":String,
    "discription":String
}
export interface plan {
    "age": String,
    "diet":Array<diet>
}
export interface diet {
    "timespan": String,
    "plan":String
}
