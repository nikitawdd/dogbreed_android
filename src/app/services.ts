import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {playlistObj,breeds,howmuchfood,healthyfood,Client,plan} from "../app/forminterfaces";

@Injectable()
export class httpService {
    constructor(private http: Http) { }

    updateProfile(obj:Client): Observable<Client> {
        return this.http
            .put("/assets/breeds.json",obj)
            .map((response: Response) => {
                return <Client>response.json();
            })
            .catch(this.handleError);
    }
    getList(): Observable<Array<playlistObj>> {
        return this.http
            .get("/assets/data.json")
            .map((response: Response) => {
                return <Array<playlistObj>>response.json();
            })
            .catch(this.handleError);
    }
    getBreeds(): Observable<Array<breeds>> {
        return this.http
            .get("/assets/breeds.json")
            .map((response: Response) => {
                return <Array<breeds>>response.json();
            })
            .catch(this.handleError);
    }
    saveBreeds(obj:breeds): Observable<Array<breeds>> {
        return this.http
            .post("/assets/breeds.json",obj)
            .map((response: Response) => {
                return <Array<breeds>>response.json();
            })
            .catch(this.handleError);
    }
    updateBreeds(obj:breeds): Observable<Array<breeds>> {
        return this.http
            .put("/assets/breeds.json",obj)
            .map((response: Response) => {
                return <Array<breeds>>response.json();
            })
            .catch(this.handleError);
    }
    deleteBreeds(id:Number): Observable<Array<breeds>> {
        return this.http
            .delete("/assets/breeds.json")
            .map((response: Response) => {
                return <Array<breeds>>response.json();
            })
            .catch(this.handleError);
    }
    gethowmuchfeed(): Observable<Array<howmuchfood>> {
        return this.http
            .get("/assets/howmuchfeed.json")
            .map((response: Response) => {
                return <Array<howmuchfood>>response.json();
            })
            .catch(this.handleError);
    }
    gethealthyfoods(): Observable<Array<healthyfood>> {
        return this.http
            .get("/assets/healthyfoods.json")
            .map((response: Response) => {
                return <Array<healthyfood>>response.json();
            })
            .catch(this.handleError);
    }
    getDietSugessions(): Observable<Array<plan>> {
        return this.http
            .get("/assets/dietplan.json")
            .map((response: Response) => {
                return <Array<plan>>response.json();
            })
            .catch(this.handleError);
    }
    private handleError(error: Response) {
        return Observable.throw(error.statusText);
      }    

}
