import { Injectable } from '@angular/core';

import { Client } from "./forminterfaces";

export class User {
    id:String;
    name:String;
    email:String;
    imageUrl:String;
    login: Boolean;
 
  constructor(obj:Client) {
    this.id = obj.id;
    this.name = obj.name;
    this.email = obj.email;
    this.imageUrl = obj.imageUrl;
    this.login = obj.login;
  }
}

@Injectable()
export class sessionService {
  currentUser: User;
 
  get():User {
    return this.currentUser;
  }
 
  set(obj:Client) {
    this.currentUser = new User(obj);  
  }
  
}
