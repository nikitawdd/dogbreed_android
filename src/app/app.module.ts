import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from "../pages/login/login";
import { trainingPage } from "../pages/training/training";
import { PlayPage } from "../pages/play/play";
import { CategoriesPage } from "../pages/categories/categories";
import { BreedsPage } from "../pages/breeds/breeds";
import { AddBreedPage } from "../pages/add-breed/add-breed";
import { DietPage } from "../pages/diet/diet";
import { FoodsPage } from "../pages/foods/foods";
import { HealthyfoodsPage } from "../pages/healthyfoods/healthyfoods";
import { EditprofilePage } from "../pages/editprofile/editprofile";
import { DietsugessionPage } from "../pages/dietsugession/dietsugession";
import { sessionService } from "../app/sessiondata";
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { httpService } from "../app/services";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    trainingPage,
    CategoriesPage,
    BreedsPage,
    AddBreedPage,
    DietPage,
    FoodsPage,
    HealthyfoodsPage,
    PlayPage,
    EditprofilePage,
    DietsugessionPage
  ],
  imports: [
    HttpClientModule,
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    trainingPage,
    CategoriesPage,
    BreedsPage,
    AddBreedPage,
    DietPage,
    FoodsPage,
    HealthyfoodsPage,
    PlayPage,
    EditprofilePage,
    DietsugessionPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Facebook,
    GooglePlus,
    sessionService,
    httpService,
    YoutubeVideoPlayer,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
