import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { plan } from "../../app/forminterfaces";
import { httpService } from "../../app/services";
/**
 * Generated class for the DietsugessionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-dietsugession',
  templateUrl: 'dietsugession.html',
})
export class DietsugessionPage {
  public dietPlan:Array<plan> = [];
  public selectedPlan:String = "";
  public showPlan:plan = {"age":"","diet":[]};
  constructor(public navCtrl: NavController, public navParams: NavParams,public http:httpService) {
    this.http.getDietSugessions().subscribe(suc=>{
      this.dietPlan = suc;
    },err=>{

    })
  }
  getData(){
    let index = this.dietPlan.findIndex(x=>x.age == this.selectedPlan);
    if(index !== -1){
      this.showPlan = this.dietPlan[index];
    }
  }

}
