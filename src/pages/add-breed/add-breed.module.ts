import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddBreedPage } from './add-breed';

@NgModule({
  declarations: [
    AddBreedPage,
  ],
  imports: [
    IonicPageModule.forChild(AddBreedPage),
  ],
})
export class AddBreedPageModule {}
