import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController} from 'ionic-angular';
import { breeds } from "../../app/forminterfaces";
import { httpService } from "../../app/services";
import { Camera, CameraOptions } from '@ionic-native/camera';

@Component({
  selector: 'page-add-breed',
  templateUrl: 'add-breed.html',
})
export class AddBreedPage {
  public breedObj:breeds = {"id": null,"name":"", "discription":"", "url":"", "lifespan":"", "temperment":"", "height":"", "weight":"", "colors":"", "origin":""};
  public mode:Boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http:httpService,public loadingCtrl: LoadingController,private camera: Camera,public alertCtrl:AlertController) {
    if(navParams.get('breed')){
      this.breedObj = navParams.get('breed');
      this.mode = true;
    } 
  }

  add() {
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    this.http.saveBreeds(this.breedObj).
    subscribe(suc=>{
      loader.dismiss();
        let alert = this.alertCtrl.create({
          subTitle: 'Breed added successuffly',
          buttons: ['Dismiss']
        });
        alert.present();    
      },err=>{
      loader.dismiss();
        let alert = this.alertCtrl.create({
          subTitle: 'Breed added successuffly',
          buttons: ['Dismiss']
        });
        alert.present();
    });
  }
  save(){
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    this.http.updateBreeds(this.breedObj).
    subscribe(suc=>{
      loader.dismiss();
        loader.dismiss();
        let alert = this.alertCtrl.create({
          subTitle: 'Breed Updated successuffly',
          buttons: ['Dismiss']
        });
        alert.present();
    },err=>{
      loader.dismiss();
        let alert = this.alertCtrl.create({
          subTitle: 'Breed Updated successuffly',
          buttons: ['Dismiss']
        });
        alert.present();
    });
  }
  takePhoto(sourceType:number) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType:sourceType,
    }

    this.camera.getPicture(options).then((imageData) => {
      this.breedObj.url = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }
}
