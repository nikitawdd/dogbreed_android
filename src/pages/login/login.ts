import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { sessionService } from "../../app/sessiondata";
import { Client } from "../../app/forminterfaces";
import { HomePage } from "../home/home";
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public ClientOj:Client = {id:null, name:"", email:"", imageUrl:"",login:false};
  constructor(public navCtrl: NavController, public navParams: NavParams, public fb: Facebook, private alertCtrl: AlertController, private googlePlus: GooglePlus,public session:sessionService, public events: Events) {
    
  }

saveCLient(){
      this.ClientOj.name  = "nikhil shende"
      this.ClientOj.email  = "nkhlshnd@gmail.com";
      this.ClientOj.id  = "12345";
      this.ClientOj.login  = true;
      this.ClientOj.imageUrl  = "http://nikiphoros.com/assets/images/company/dp.jpg";
      this.session.set(this.ClientOj);
      this.events.publish('user:created', "user", Date.now());
      this.navCtrl.setRoot(HomePage);
}
  loginGoogle() {
    this.googlePlus.login({})
      .then(res => {
        this.saveCLient();
      })
      .catch(e =>{
          let alert = this.alertCtrl.create({
              title: 'Error logging into Facebook',
              subTitle: JSON.stringify(e)+' ',
              buttons: ['Dismiss']
            });
            alert.present();
            this.saveCLient();
      });
  }
  loginFacebook(){
    // Login with permissions
    this.fb.login(['public_profile', 'email'])
    .then( (res: FacebookLoginResponse) => {
        // The connection was successful
        if(res.status == "connected") {
            // Get user infos from the API
            this.fb.api("/me?fields=name,gender,birthday,email", []).then((user) => {
                // Get the connected user details
    //             var gender    = user.gender;
    //             var birthday  = user.birthday;
    //             var name      = user.name;
    //             var email     = user.email;
                  this.saveCLient();
            });
        } 
        // An error occurred while loging-in
        else {           
            let alert = this.alertCtrl.create({
              title: 'Error logging into Facebook',
              subTitle: 'error while login',
              buttons: ['Dismiss']
            });
            alert.present();
        }
    }).catch((e) => {
      let alert = this.alertCtrl.create({
        title: 'Error logging into Facebook',
        subTitle: JSON.stringify(e)+' ',
        buttons: ['Dismiss']
      });
      alert.present();
      this.saveCLient();
    });
}

}
