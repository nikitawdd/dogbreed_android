import { Component} from '@angular/core';
import { NavController } from 'ionic-angular';
import { playlistObj } from "../../app/forminterfaces";
import { httpService } from "../../app/services";
import { PlayPage } from "../play/play";
import { LoginPage } from "../login/login";
import { AlertController } from 'ionic-angular';
@Component({
  selector: 'page-training',
  templateUrl: 'training.html'
})
export class trainingPage{
  public Playlist:Array<playlistObj> = [];
  public isLoggedIn:boolean = false;
  public error:any;
  constructor(public navCtrl: NavController,public http:httpService,private alertCtrl: AlertController) {
    this.http.getList().subscribe(suc=>{
      this.Playlist = suc;
    },error=>{
      console.log(error);
    });
  }

  playVideo(obj:playlistObj){
    this.navCtrl.push(PlayPage, {
      video: obj
    });
  }
}
