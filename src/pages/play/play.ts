import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { playlistObj } from "../../app/forminterfaces";
import { DomSanitizer } from '@angular/platform-browser';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

/**
 * Generated class for the PlayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-play',
  templateUrl: 'play.html',
})
export class PlayPage {
  public video:playlistObj;
  public videoUrl;
  constructor(public navCtrl: NavController, public navParams: NavParams,public sanitizer: DomSanitizer,private youtube: YoutubeVideoPlayer) {
    this.video = navParams.get('video');
    this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/"+this.video.resourceId.videoId);
  }

  openUrl(){
    this.youtube.openVideo(""+this.video.resourceId.videoId);
  }
}
