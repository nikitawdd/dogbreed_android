import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { howmuchfood } from "../../app/forminterfaces";
import { httpService } from "../../app/services";
@Component({
  selector: 'page-foods',
  templateUrl: 'foods.html'
})
export class FoodsPage {
  public foodslists:Array<howmuchfood> = [];
  constructor(public navCtrl: NavController,public http:httpService) {
    this.http.gethowmuchfeed().subscribe(suc=>{
      this.foodslists = suc;
    },error=>{
      console.log(error);
    });
  }

}
