import { Component} from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from "../login/login";
import { Storage } from '@ionic/storage';
import { FoodsPage } from "../foods/foods";
import { HealthyfoodsPage } from "../healthyfoods/healthyfoods";
@Component({
  selector: 'page-diet',
  templateUrl: 'diet.html'
})
export class DietPage{
  public isLoggedIn:boolean = false;
  public error:any;
  constructor(public navCtrl: NavController) {
  }
  
  gotoHowMuchfeeds(){
    this.navCtrl.push(FoodsPage);
  }
  gotoHealthyFoods(){
    this.navCtrl.push(HealthyfoodsPage);
  }
}
