import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { healthyfood } from "../../app/forminterfaces";
import { httpService } from "../../app/services";
@Component({
  selector: 'page-healthyfoods',
  templateUrl: 'healthyfoods.html'
})
export class HealthyfoodsPage {
  public foodslist:Array<healthyfood> = [];
  constructor(public navCtrl: NavController,public http:httpService) {
    this.http.gethealthyfoods().subscribe(suc=>{
      this.foodslist = suc;
    },error=>{
      console.log(error);
    });
  }

}
