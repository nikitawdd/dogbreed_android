import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { breeds } from "../../app/forminterfaces";
@Component({
  selector: 'page-breeds',
  templateUrl: 'breeds.html'
})
export class BreedsPage {
  public dog:breeds;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
     this.dog = navParams.get('video');
  }

}
