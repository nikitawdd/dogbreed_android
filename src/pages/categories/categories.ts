import { Component} from '@angular/core';
import { NavController } from 'ionic-angular';
import { breeds } from "../../app/forminterfaces";
import { httpService } from "../../app/services";
import { BreedsPage } from "../breeds/breeds";
import { LoginPage } from "../login/login";
import { AlertController } from 'ionic-angular';
import { AddBreedPage } from "../add-breed/add-breed";
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html'
})
export class CategoriesPage{
  public Breedlist:Array<breeds> = [];
  public isLoggedIn:boolean = false;
  public error:any;
  constructor(public navCtrl: NavController,public http:httpService,private alertCtrl: AlertController) {
    this.http.getBreeds().subscribe(suc=>{
      this.Breedlist = suc;
    },error=>{
      console.log(error);
    });
  }

  viewBreed(obj:breeds){
    this.navCtrl.push(BreedsPage, {
      video: obj
    });
  }
  editBreed(item:breeds){
    this.navCtrl.push(AddBreedPage, {
      breed: item
    });
  }
  addBreed(){
    this.navCtrl.push(AddBreedPage);
  }
  deleteBreed(id:Number){

      let alert = this.alertCtrl.create({
      title: 'Confirm Purpose',
      message: 'Do you want to delete this Breed?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Yes',
          handler: () => {
          this.http.deleteBreeds(id)
          .subscribe(suc=>{
            let alertsuc = this.alertCtrl.create({
              title: 'Breed deleted successfully',
              buttons: ['Dismiss']
            });
            alertsuc.present();
          },err =>{

          });
            
          }
        }
      ]
    });
    alert.present();
  }
}
