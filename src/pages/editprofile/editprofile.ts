import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController} from 'ionic-angular';
import { sessionService } from "../../app/sessiondata";
import { Client } from "../../app/forminterfaces";
import { httpService } from "../../app/services";
import { Camera, CameraOptions } from '@ionic-native/camera';

@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditprofilePage {
   private ClientObj:Client = {id:null, name:"", email:"", imageUrl:"",login:false};
  constructor(public navCtrl: NavController, public navParams: NavParams,public session:sessionService,public http:httpService,public loadingCtrl: LoadingController,private camera: Camera,public alertCtrl:AlertController) {
    this.ClientObj= this.session.get();
  }
  save(){
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    this.http.updateProfile(this.ClientObj).
    subscribe(suc=>{
        loader.dismiss();
        let alert = this.alertCtrl.create({
          subTitle: 'Profile Updated successuffly',
          buttons: ['Dismiss']
        });
        alert.present();
        this.session.set(this.ClientObj);
    },err=>{
      loader.dismiss();
        let alert = this.alertCtrl.create({
          subTitle: 'Profile Updated successuffly',
          buttons: ['Dismiss']
        });
        alert.present();
        this.session.set(this.ClientObj);
    });
  }
  takePhoto(sourceType:number) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType:sourceType,
    }

    this.camera.getPicture(options).then((imageData) => {
      this.ClientObj.imageUrl = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }
}
