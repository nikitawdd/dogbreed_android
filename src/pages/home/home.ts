import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { sessionService } from "../../app/sessiondata";
import { trainingPage } from "../training/training";
import { CategoriesPage } from "../categories/categories";
import { DietPage } from "../diet/diet";
import { DietsugessionPage } from "../dietsugession/dietsugession";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,public session:sessionService) {

  }
  goToDietPage(){
    this.navCtrl.push(DietPage);
  }
  goTotrainingPage(){
    this.navCtrl.push(trainingPage);
  }
  goToCategoriesPage(){
    this.navCtrl.push(CategoriesPage);
  }
  goToDietSugPage(){
    this.navCtrl.push(DietsugessionPage);
  }
}
